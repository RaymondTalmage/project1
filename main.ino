#include <stdio.h>
#define t_LED 2
#define t_ON 3
#define t_OFF 4
#define t_RED 5
#define t_GREEN 6
#define t_BLINK 7
#define t_STATUS 8
#define t_VERSION 9
#define t_HELP 10
#define t_LEDS 11
#define t_D13 12
#define t_SET 13
#define t_WORD 14
#define t_ERR 15
#define t_EOL 255

//Table of tokens
static char tok_table[13][4]= {'l', 'e', 3, t_LED,
                      'o', 'n', 2, t_ON,
                      'o', 'f', 3, t_OFF,
                      'r', 'e', 3, t_RED,
                      'g', 'r', 5, t_GREEN,
                      'b', 'l', 5, t_BLINK,
                      's', 't', 6, t_STATUS,
                      'v', 'e', 7, t_VERSION,
                      'h', 'e', 4, t_HELP,
                      'l', 'e', 4, t_LEDS,
                      's', 'e', 3, t_SET,
                      'd', '1', 3, t_D13};

//Current amount of tokens
static int token_amount = 13;

unsigned long j0interval;
int job0vec;
int job1vec;
int job2vec;
unsigned long j1interval;
unsigned long j2interval;
int inx = 0;
char* buff;
unsigned long ct;

unsigned int led_blink;

unsigned int d13_blink;

//set to one if green was last pin turned on otherwise 0
int last_color = 0;

//Variable to store the delay between blinks
int blink_time_dual;

int blink_time_d13;

unsigned int blink_rate;

//Buffer for tokens loaded
char tok_buffer[4];

unsigned char display_buffer;

unsigned char d13 = 0;

char input_buffer[40];

//Current iteration of commands
int version = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  j0init();
  j1init();
 // help();
  j2init();
}

void loop(){
  ct = millis();
  if(ct>j0interval){
    job0();
  }
  if(ct>j1interval){
    job1();
  }
  if(ct>j2interval){
    job2();
  }
}

void j0init(){
  j0interval = millis() + 100;
  job0vec = 0;
  inx = 0;
}

void job0(){
  //j0init();
  switch(job0vec){
    case 0:
      inx = 0;
      job0vec++;
      Serial.print('>');
      break;
     case 1:
     char c;
       while(Serial.available() > 0){
        c = Serial.read();
         if(c != '\r'){
          if(c == 127 && inx != 0){
            Serial.print('\n');
            input_buffer[inx]='\0';
            inx--;
            input_buffer[inx]='\0';
            Serial.println(input_buffer);
          }
          else{
            input_buffer[inx] = c;
            input_buffer[inx + 1] = '\0';
            inx++;
            Serial.print(c);
          }
        }
         //Serial.println(input_buffer[inx -1]);
       }
       buff = input_buffer;
       //Serial.println(input_buffer);
       if(c == '\r'){
        job0vec++;
       }
       break;
     case 2:
       Serial.print('\n');
       tokenizer(buff);
       job0vec++;
       break;
     case 3:
       token_intepreter();
       job0vec++;
       break;
     case 4:
       job0vec = 0;
       break;
     default:
       job0vec = 0;
       break;
  }
  j0interval+=100;
}

void j1init(){
  j1interval = millis() + 500;
  job1vec = 0;
  display_buffer = 0;
  led_blink = 500;
}

void job1(){
  switch(job1vec){
    case 0:
      digitalWrite(6, (display_buffer)&(1));
      digitalWrite(7, (display_buffer>>1)&(1));
      display_buffer = rotate_byte_left(display_buffer, 2);
      //display_buffer = rotate_byte_right(display_buffer);
      break;
    default:
      job1vec = 0;
      break;
  }
  job1vec=0;
  j1interval+=led_blink;
}

void j2init(){
  j2interval = millis() + 500;
  job2vec = 0;
  d13_blink = 500;
}

void job2(){
  digitalWrite(LED_BUILTIN, (d13)&(1));
  d13 = rotate_byte_left(d13, 1);
  j2interval+=d13_blink;
}

unsigned char rotate_byte_left(unsigned char b, int n){
  return (b >> n) |(b << (8 - n));
}

unsigned char rotate_byte_right(unsigned char b){
  return (b << 2) | (b >> 6);
}
//Takes the current input buffer and scans for tokens
//If a token is found it is put on the token buffer
void tokenizer(char* index){
  //char *index = input_buffer;
  int letter_count = 0;
  int token_count = 0;
  char *reset_index;
  while(*index != '\0'){
    letter_count = 0;
    if(*index == ' '){
      index++;
      //Serial.println("space hit");
    }
    else{
      reset_index = index;
      for(int i = 0; i < token_amount; i++){
        if(tok_table[i][0] == *index){
          index++;
          letter_count++;
          //Serial.println("first letter hit");
          if(tok_table[i][1] == *index){
            while(*index != ' ' && *index != '\0'){
              letter_count++;
              index++;
            }
            //Serial.println("second letter hit");
            //Serial.println(letter_count);
            if(tok_table[i][2] == letter_count){
              tok_buffer[token_count] = tok_table[i][3];
              //Serial.println("Token");
              token_count++;
              if(tok_buffer[token_count-1] == t_BLINK && tok_buffer[token_count -2] == t_SET){
                tok_buffer[token_count] = t_WORD;
                token_count++;
                index++;
                blink_rate = 0;
                char temp[5];
                int i = 0;
                while(*index != ' ' && *index!='\0'){
                  temp[i] = *index;
                  index++;
                  i++;
                }
                int mult = 1;
                for(int j = i-1; j >= 0;j--){
                  blink_rate = blink_rate + ((int)temp[j] - 48) * mult;
                  mult = mult *10;
                }
               // Serial.print(blink_rate);
                
              }
              break;
            }
          }
        }
        letter_count = 0;
        index = reset_index;
      }
      while(*index != ' ' && *index != '\0'){
        index++;
      }
    }
    
  }
  version++;
  tok_buffer[token_count] = t_EOL;
  //Serial.println(version);
  
}

void token_intepreter(){
  char *tok_pos = tok_buffer;
  switch(*tok_pos){
    case t_LED:
      tok_pos++;
      switch(*tok_pos){
        case t_RED:
          display_buffer = 170;
          tok_pos++;
          last_color = 0;
          switch(*tok_pos){
            case t_GREEN:
              tok_pos++;
              switch(*tok_pos){
                case t_BLINK:
                  display_buffer = 153;
                  break;
              }
          }
          break;
        case t_GREEN:
          display_buffer = 85;
          tok_pos++;
          last_color = 1;
          switch(*tok_pos){
            case t_RED:
              tok_pos++;
              switch(*tok_pos){
                case t_BLINK:
                  display_buffer = 153;
                  break;
              }
              break;
          }
          break;
        case t_SET:
          tok_pos++;
          switch(*tok_pos){
            case t_BLINK:
              led_blink = blink_rate;
              break;
          }
          break;
        case t_BLINK:
          switch(last_color){
            case 1:
              display_buffer = 68;
              break;
            case 0:
              display_buffer = 136;
              break;
            default: 
              display_buffer = 136;
              break;
          }
          break;
        case t_OFF:
          display_buffer = 0;
          break;
      }
      case t_D13:
        tok_pos++;
        switch(*tok_pos){
          case t_ON:
            d13 = 255;
            break;
          case t_OFF:
            d13 = 0;
            break;
          case t_BLINK:
            d13 = 170;
            break;
          case t_SET:
            tok_pos++;
            switch(*tok_pos){
              case t_BLINK:
              d13_blink = blink_rate;
              break;
            }
            break;
        }
      case t_STATUS:
        tok_pos++;
        switch(*tok_pos){
          case t_LEDS:
            status_leds();
            break;
        }
        break;
      case t_VERSION:
        Serial.println(version);
        break;
      case t_HELP:
        help();
        break;
  }
}

void status_leds(){
  switch(d13){
    case 170:
      Serial.println("Onboard LED blinking");
      break;
    case 85:
      Serial.println("Onboard LED blinking");
      break;
    case 255:
      Serial.println("Onboard LED on");
      break;
    case 0:
      Serial.println("Onboard LED off");
      break;
  }
  switch(display_buffer){
    case 85:
      Serial.println("Dual LED green");
      break;
    case 0:
      Serial.println("Dual LED off");
      break;
    case 170:
      Serial.println("Dual LED red");
      break;
    case 153:
      Serial.println("Dual LED blinking red green");
      break;
    case 102:
      Serial.println("Dual LED blinking red green");
      break;
    case 136:
      Serial.println("Dual LED blinking red");
      break;
    case 68:
      Serial.println("Dual LED blinking green");
      break;
    
  }
}

void help(){
  Serial.println("List of some commands, enter what you wan to control and then an option:" );
  Serial.println("D13 for the on board LED: ON/OFF");
  Serial.println("LED for the bicolor LED: GREEN/RED/OFF/BLINK");
  Serial.println("SET BLINK to set the amount of time in miliseconds between each blink");
  Serial.println("STATUS LED to check the current status of each LED");
  Serial.println("VERSION to check the current iteration of your commands");
  Serial.println("HELP to access this help menu");
}
