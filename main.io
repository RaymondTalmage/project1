#include <stdio.h>
#define t_LED 2
#define t_ON 3
#define t_OFF 4
#define t_RED 5
#define t_GREEN 6
#define t_BLINK 7
#define t_STATUS 8
#define t_VERSION 9
#define t_HELP 10
#define t_LEDS 11
#define t_D13 12
#define t_SET 13
#define t_WORD 14
#define t_ERR 15
#define t_EOL 255

//Table of tokens
static char tok_table[13][4]= {'l', 'e', 3, t_LED,
                      'o', 'n', 2, t_ON,
                      'o', 'f', 3, t_OFF,
                      'r', 'e', 3, t_RED,
                      'g', 'r', 5, t_GREEN,
                      'b', 'l', 5, t_BLINK,
                      's', 't', 6, t_STATUS,
                      'v', 'e', 7, t_VERSION,
                      'h', 'e', 4, t_HELP,
                      'l', 'e', 4, t_LEDS,
                      's', 'e', 3, t_SET,
                      'd', '1', 3, t_D13};

//Current amount of tokens
static int token_amount = 13;

//set to one if green was last pin turned on, 2 for alternating blink pattern, otherwise 0
int last_pin; 

//Variable to store the delay between blinks
int blink_time;

//Buffer for tokens loaded
char tok_buffer[4];

//Current iteration of commands
int version = 0;

//Sets necessary pins to output and opens serial communications for app
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  blink_time = 500;
  help();
}

void loop() {
  // put your main code here, to run repeatedly:
  char input_buffer[40];
  int n = 0;

  //Reads input from user until endline is detected
  while(Serial.available() > 0){
    n = Serial.readBytesUntil('\n', input_buffer, 40);
    input_buffer[n] = '\0';
    char *index = input_buffer;
    tokenizer(index);
  }
  /*
    Main loop that iterates through the current token buffer 
    when it reaches the correct token it will iterate forward
    and check the next token. Once it has reached a point where
    a command can be implemented it will call the appropriate function
  */
  char *tok_pos = tok_buffer;
  switch(*tok_pos){
    case t_LED:
      tok_pos++;
      switch(*tok_pos){
        case t_RED:
          led_red();
          last_pin = 0;
          tok_pos++;
          switch(*tok_pos){
            case t_GREEN:
              tok_pos++;
              switch(*tok_pos){
                case t_BLINK:
                  led_blink(2); 
                  break; 
              }
            break;
          }
          break;
        case t_GREEN:
          led_green();
          last_pin = 1;
          tok_pos++;
          switch(*tok_pos){
            case t_RED:
              tok_pos++;
              switch(*tok_pos){
                case t_BLINK:
                  led_blink(2); 
                  break; 
              }
            break;
          }
          break;
        case t_BLINK:
          led_blink(last_pin);
          break;
        case t_OFF:
          led_off();
          break;
      }
      break;
    case t_D13:
      tok_pos++;
      switch(*tok_pos){
        case t_ON:
          built_in_led_on();
          break;
        case t_OFF:
          built_in_led_off();
          break;
        case t_BLINK:
          built_in_led_blink();
          break;
      }
      break;
    case t_STATUS:
      tok_pos++;
      switch(*tok_pos){
        case t_LEDS:
          status_leds();
          tok_buffer[0] = t_EOL;
          break;
      }
      break;
    case t_VERSION:
      Serial.println(version);
      tok_buffer[0] = t_EOL;
      break;
    case t_SET:
      break;
    case t_HELP:
      help();
      tok_buffer[0] = t_EOL;
      break;
  }

  
}
//Takes the current input buffer and scans for tokens
//If a token is found it is put on the token buffer
void tokenizer(char* index){
  //char *index = input_buffer;
  int letter_count = 0;
  int token_count = 0;
  char *reset_index;
  while(*index != '\0'){
    letter_count = 0;
    if(*index == ' '){
      index++;
      Serial.println("space hit");
    }
    else{
      reset_index = index;
      for(int i = 0; i < token_amount; i++){
        if(tok_table[i][0] == *index){
          index++;
          letter_count++;
          Serial.println("first letter hit");
          if(tok_table[i][1] == *index){
            while(*index != ' ' && *index != '\0'){
              letter_count++;
              index++;
            }
            Serial.println("second letter hit");
            Serial.println(letter_count);
            if(tok_table[i][2] == letter_count){
              tok_buffer[token_count] = tok_table[i][3];
              Serial.println("Token");
              token_count++;
              if(tok_buffer[token_count-1] == t_SET){
                tok_buffer[token_count] = t_WORD;
                token_count++;
              }
              break;
            }
          }
        }
        letter_count = 0;
        index = reset_index;
      }
      while(*index != ' ' && *index != '\0'){
        index++;
      }
    }
    
  }
  version++;
  tok_buffer[token_count] = t_EOL;
  Serial.println(version);
  
}

/*void word_write_to_buffer(*index){
  while(*index!= '\0'){
    switch(*index){
      case ' ':
        index++;
      default:
        
    }
  }
}*/
// Turn bi color led to red
void led_red(){
  digitalWrite(7, HIGH);
  digitalWrite(6, LOW);
}
//Turn bi color led to green
void led_green(){
  digitalWrite(7, LOW);
  digitalWrite(6, HIGH);
}
//Turn bi color led off
void led_off(){
  digitalWrite(7, LOW);
  digitalWrite(6, LOW);
}
//turn built in led on
void built_in_led_on(){
  digitalWrite(LED_BUILTIN, HIGH);
}
//Turn built in led off
void built_in_led_off(){
  digitalWrite(LED_BUILTIN, LOW);
}
//Check the current status of the led lights
void status_leds(){
  int leds[] = {digitalRead(7), digitalRead(6), digitalRead(LED_BUILTIN)};
  switch(leds[0]){
    case 1:
      switch(leds[1]){
        case 0:
          Serial.println("Dual LED color red");
          break;
        case 1:
          Serial.println("Dual LED OFF");
          break;
      }
      break;
      case 0:
        switch(leds[1]){
          case 1:
            Serial.println("Dual LED color green");
            break;
          case 0:
            Serial.println("Dual LED OFF");
            break;
        }
        break;
  }
  switch(leds[2]){
    case 1:
      Serial.println("Onboard LED ON");
      break;
    case 0:
      Serial.println("On board LED OFF");
      break;
  }
}
//Change the built in led to blinking
void built_in_led_blink(){
  built_in_led_on();
  delay(blink_time);
  built_in_led_off();
  delay(blink_time);
}
//Change the bi color led to blinking, blinks as last color or defaults red
void led_blink(int last_pin){
  switch(last_pin){
    case 1:
      led_green();
      delay(blink_time);
      led_off();
      delay(blink_time);
      break;
    case 2:
      led_green();
      last_pin = 1;
      delay(blink_time);
      led_off();
      delay(blink_time);
      led_red();
      last_pin = 0;
      delay(blink_time);
      led_off();
      delay(blink_time);
      break;
    default:
      led_red();
      delay(blink_time);
      led_off();
      delay(blink_time);
      break;      
  }
}

void set_blink(int blink_set){
  blink_time = blink_set;
}

void help(){
  Serial.println("List of some commands, enter what you wan to control and then an option:" );
  Serial.println("D13 for the on board LED: ON/OFF");
  Serial.println("LED for the bicolor LED: GREEN/RED/OFF/BLINK");
  Serial.println("SET BLINK to set the amount of time in miliseconds between each blink");
  Serial.println("STATUS LED to check the current status of each LED");
  Serial.println("VERSION to check the current iteration of your commands");
  Serial.println("HELP to access this help menu");
}