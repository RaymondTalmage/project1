#include <stdio.h>
#include <DS3231.h>
#include <Wire.h>
#include <EEPROM.h>
#include <FastLED.h>
#include <SimpleDHT.h>

#define t_LED 2
#define t_ON 3
#define t_OFF 4
#define t_RED 5
#define t_GREEN 6
#define t_BLINK 7
#define t_STATUS 8
#define t_VERSION 9
#define t_HELP 10
#define t_LEDS 11
#define t_D13 12
#define t_SET 13
#define t_WORD 14
#define t_TIME 15
#define t_GET 16
#define t_COLOR 17
#define t_ADD 18
#define t_RGB 19
#define t_HUMIDITY 20
#define t_TEMPERATURE 21
#define t_HISTORY 22
#define t_MAXMIN 23
#define t_FAREN 24
#define t_ERR 25
#define t_EOL 255

//Table of token
static char tok_table[22][4] = {'l', 'e', 3, t_LED,
                                'o', 'n', 2, t_ON,
                                'o', 'f', 3, t_OFF,
                                'r', 'e', 3, t_RED,
                                'g', 'r', 5, t_GREEN,
                                'b', 'l', 5, t_BLINK,
                                's', 't', 6, t_STATUS,
                                'v', 'e', 7, t_VERSION,
                                'h', 'e', 4, t_HELP,
                                'l', 'e', 4, t_LEDS,
                                's', 'e', 3, t_SET,
                                't', 'i', 4, t_TIME,
                                'g', 'e', 3, t_GET,
                                'c', 'o', 5, t_COLOR,
                                'a', 'd', 3, t_ADD,
                                'd', '1', 3, t_D13,
                                'r', 'g', 3, t_RGB,
                                'h', 'u', 8, t_HUMIDITY,
                                't', 'e', 11, t_TEMPERATURE,
                                'h', 'i', 7, t_HISTORY,
                                'm', 'a', 6, t_MAXMIN,
                                'f', 'a', 5, t_FAREN
                               };


//Current amount of tokens
static int token_amount = 22;
bool century = false;
unsigned long input_tm;
int input_parservec;
int led_jobvec;
int d13_jobvec;
int rgb_jobvec;
unsigned long led_interval;
unsigned long d13_interval;
unsigned long rgb_interval;
unsigned long save_temp_interval;
unsigned long save_temp_hum_interval;
int inx = 0;
char* buff;
unsigned long ct;
byte temperature;
byte humidity;

byte red;

byte green;

byte blue;

bool rgb_pattern;

bool h12;
bool PM;

unsigned int led_blink; //Delay for the blink on the LED

unsigned int d13_blink; //Delay for the blink on the built in LED

unsigned int rgb_blink;

//set to one if green was last pin turned on otherwise 0
int last_color = 0;

//Variable to store the delay between blinks
int blink_time_dual;

int blink_time_d13;

bool rgb_blinking;

unsigned int blink_rate;

//Buffer for tokens loaded
char tok_buffer[4];

unsigned char display_buffer;

unsigned char d13 = 0;

char input_buffer[40];

DS3231 rtc;

CRGB led[1];

SimpleDHT22 dht22(2);

//Current iteration of commands
int version = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Wire.begin();
  rtc.setClockMode(false);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  input_parser_init();
  led_init();
  d13_init();
  save_temp_init();
  
  save_temp_hum_init();
  help();
  //  rtc.begin();
  rgb_init();
  //set_time(0, 1, 0, 1, 1, 2, 0, 0, 0, 0);
  FastLED.addLeds<NEOPIXEL, 3>(led, 1);

}

void loop() {
  ct = millis();

  if (ct > input_tm) {
    input_parser();
  }
  if (ct > led_interval) {
    led_job();
  }
  if (ct > d13_interval) {
    d13_job();
  }
 /* if (ct > save_temp_interval) {
    save_temp_hum();
    delay(100);
    save_ts_temp_hum();
    eeprom_read();
    //print_temp_humidity();
    //print_time();
  }
  if(ct > save_temp_hum_interval){
    save_ts_temp_hum();
  }*/
  if(ct > rgb_interval){
    rgb_job();
  }
  if(ct > save_temp_interval){
    save_temp_hum();
  }
  if(ct > save_temp_hum_interval){
    save_ts_temp_hum();
  }

}

void input_parser_init() {
  input_tm = millis() + 100;
  input_parservec = 0;
  inx = 0;
}

void input_parser() {
  //j0init();
  switch (input_parservec) {
    case 0:
      inx = 0;
      input_parservec++;
      Serial.print('>');
      break;
    case 1:
      char c;
      while (Serial.available() > 0) {
        c = Serial.read();
        if (c != '\r') {
          if (c == 127 && inx != 0) {
            Serial.print('\b');
            Serial.print(' ');
            Serial.print('\b');
            input_buffer[inx] = ' ';
            inx--;
            input_buffer[inx] = ' ';
            //Serial.println(input_buffer);
          }
          else {
            input_buffer[inx] = c;
            input_buffer[inx + 1] = '\0';
            inx++;
            Serial.print(c);
          }
        }
        //Serial.println(input_buffer[inx -1]);
      }
      buff = input_buffer;
      //Serial.println(input_buffer);
      if (c == '\r') {
        input_parservec++;
        Serial.print('\r');
      }
      break;
    case 2:
      Serial.print('\n');
      tokenizer(buff);
      input_parservec++;
      break;
    case 3:
      token_intepreter();
      input_parservec++;
      break;
    case 4:
      input_parservec = 0;
      break;
    default:
      input_parservec = 0;
      break;
  }
  input_tm += 100;
}

void led_init() {
  led_interval = millis() + 500;
  led_jobvec = 0;
  display_buffer = 0;
  led_blink = 500;
}

void save_temp_hum_init(){
  //EEPROM[0] += 2;
  //save_ts_temp_hum();
  save_temp_hum_interval = millis() + 6000;
}

void save_temp_init(){
  save_temp_hum();
  save_temp_interval = millis() + 5000;
}

void led_job() {
  switch (led_jobvec) {
    case 0:
      digitalWrite(6, (display_buffer) & (1));
      digitalWrite(7, (display_buffer >> 1) & (1));
      display_buffer = rotate_byte_left(display_buffer, 2);
      //display_buffer = rotate_byte_right(display_buffer);
      break;
    default:
      led_jobvec = 0;
      break;
  }
  led_jobvec = 0;
  led_interval += led_blink;
}

void rgb_init(){
  rgb_jobvec = 0;
  rgb_interval = millis() + 500;
  rgb_blink = 500;
}

void rgb_job(){
  switch(rgb_jobvec){
    case 0:
      //code to light led based on input values
      switch(rgb_blinking){
        case false:
          led[0] = CRGB(red, green, blue);
          FastLED.show();
          break;
        case true:
          led[0]= CRGB(red*rgb_pattern, green*rgb_pattern, blue*rgb_pattern);
          FastLED.show();
          rgb_pattern = (rgb_pattern == false) * 1;
          break;
      }
      break;
    default:
      rgb_jobvec = 0;
      rgb_interval += rgb_blink;
      break;
  }
  rgb_interval += rgb_blink;
  rgb_jobvec = 0;
}

void d13_init() {
  d13_interval = millis() + 500;
  d13_jobvec = 0;
  d13_blink = 500;
}

void d13_job() {
  digitalWrite(LED_BUILTIN, (d13) & (1));
  d13 = rotate_byte_left(d13, 1);
  d13_interval += d13_blink;
}

unsigned char rotate_byte_left(unsigned char b, int n) {
  return (b >> n) | (b << (8 - n));
}

unsigned char rotate_byte_right(unsigned char b) {
  return (b << 2) | (b >> 6);
}
//Takes the current input buffer and scans for tokens
//If a token is found it is put on the token buffer
void tokenizer(char* index) {
  //char *index = input_buffer;
  int letter_count = 0;
  int token_count = 0;
  char *reset_index;
  while (*index != '\0') {
    letter_count = 0;
    if (*index == ' ') {
      index++;
      //Serial.println("space hit");
    }
    else {
      reset_index = index;
      for (int i = 0; i < token_amount; i++) {
        if (tok_table[i][0] == *index) {
          index++;
          letter_count++;
          //Serial.println("first letter hit");
          if (tok_table[i][1] == *index) {
            while (*index != ' ' && *index != '\0') {
              letter_count++;
              index++;
            }
            //Serial.println("second letter hit");
            //Serial.println(letter_count);
            if (tok_table[i][2] == letter_count) {
              tok_buffer[token_count] = tok_table[i][3];
              //Serial.println("Token");
              token_count++;
              if (tok_buffer[token_count - 1] == t_BLINK && tok_buffer[token_count - 2] == t_SET) {
                tok_buffer[token_count] = t_WORD;
                token_count++;
                index++;
                blink_rate = 0;
                char temp[5];
                int i = 0;
                while (*index != ' ' && *index != '\0') {
                  temp[i] = *index;
                  index++;
                  i++;
                }
                int mult = 1;
                for (int j = i - 1; j >= 0; j--) {
                  blink_rate = blink_rate + ((int)temp[j] - 48) * mult;
                  mult = mult * 10;
                }
                // Serial.print(blink_rate);

              }
              if(tok_buffer[token_count -1] == t_COLOR && tok_buffer[token_count -2] == t_SET && tok_buffer[token_count -3] == t_RGB){
                int color[3];
                char temp[3];
                long value = 0;
                int colors = 0;
                int mult = 1;
                int inx = 0;
                //index++;
                while(*index != '\0' && *index != '\n' && colors < 3){
                  if(*index != ' '){
                  while(*index != ' ' && *index != '\0'){
                    temp[inx] = *index;
                    //Serial.print(temp[inx]);
                    inx++;
                    index++;  
                  }
                  for (int j = inx - 1; j >= 0; j--) {
                    value = value + ((int)temp[j] - 48) * mult;
                    mult = mult * 10;
                  }
                  Serial.println(value);
                  if(value > 255 || inx > 3){ value = 255; }
                  color[colors] = value;
                  colors++;
                  mult = 1;
                  inx = 0;
                  value = 0;
                  }
                  index++;
                }
                
                red = color[0];
                Serial.println(red);
                green= color[1];
                Serial.println(green);
                blue = color[2];
                Serial.println(blue);
              }
              if(tok_buffer[token_count - 1] == t_ADD){
                int number1 = 0;
                int number2 = 0;
                char n1[7];
                char n2[7];
                int i = 0;
                int mult = 1;
                index++;
                while(*index != ' '){
                      n1[i] = *index;
                      i++;
                      index++;
                }
                for (int j = i - 1; j >= 0; j--) {
                  number1 = number1 + ((int)n1[j] - 48) * mult;
                  mult = mult * 10;
                }
                mult = 1;
                i = 0;
                index++;
                while(*index != '\0' && *index != ' '){
                      n2[i] = *index;
                      i++;
                      index++;
                }
                for (int j = i - 1; j >= 0; j--) {
                  number2 = number2 + ((int)n2[j] - 48) * mult;
                  mult = mult * 10;
                }

                Serial.print(number1);Serial.print(" + "); Serial.print(number2);Serial.print(" = "); Serial.println(number1 + number2);
                
              }
              if(tok_buffer[token_count -1] == t_TIME && tok_buffer[token_count - 2] == t_SET){
                char times[10];
                Serial.println("here");
                int i = 0;
                while(*index != '\0' && i <= 10){
                  while(*index != ' ' && *index != '\0' && *index != '\n'){
                    times[i] = *index;
                    Serial.println(times[i]);
                    i++;
                    index++;
                  }
                  index++;
                }
                if(i <=10){
                  set_time(char(times[0]), char(times[1]), times[2], times[3], times[4], times[5], times[6], times[7], times[8], times[9]);
                }
              }
              break;
            }
          }
        }
        letter_count = 0;
        index = reset_index;
      }
      while (*index != ' ' && *index != '\0') {
        index++;
      }
    }

  }
  version++;
  tok_buffer[token_count] = t_EOL;
  //Serial.println(version);

}

void token_intepreter() {
  char *tok_pos = tok_buffer;
  switch (*tok_pos) {
    case t_LED:
      tok_pos++;
      switch (*tok_pos) {
        case t_RED:
          display_buffer = 170;
          tok_pos++;
          last_color = 0;
          switch (*tok_pos) {
            case t_GREEN:
              tok_pos++;
              switch (*tok_pos) {
                case t_BLINK:
                  display_buffer = 153;
                  break;
              }
          }
          break;
        case t_GREEN:
          display_buffer = 85;
          tok_pos++;
          last_color = 1;
          switch (*tok_pos) {
            case t_RED:
              tok_pos++;
              switch (*tok_pos) {
                case t_BLINK:
                  display_buffer = 153;
                  break;
              }
              break;
          }
          break;
        case t_SET:
          tok_pos++;
          switch (*tok_pos) {
            case t_BLINK:
              led_blink = blink_rate;
              break;
          }
          break;
        case t_BLINK:
          switch (last_color) {
            case 1:
              display_buffer = 68;
              break;
            case 0:
              display_buffer = 136;
              break;
            default:
              display_buffer = 136;
              break;
          }
          break;
        case t_OFF:
          display_buffer = 0;
          break;
      }
    case t_D13:
      tok_pos++;
      switch (*tok_pos) {
        case t_ON:
          d13 = 255;
          break;
        case t_OFF:
          d13 = 0;
          break;
        case t_BLINK:
          d13 = 170;
          break;
        case t_SET:
          tok_pos++;
          switch (*tok_pos) {
            case t_BLINK:
              d13_blink = blink_rate;
              break;
          }
          break;
      }
    case t_STATUS:
      tok_pos++;
      switch (*tok_pos) {
        case t_LEDS:
          status_leds();
          break;
      }
      break;
    case t_RGB:
      tok_pos++;
      switch(*tok_pos){
        case t_OFF:
          red = 0;
          green = 0;
          blue = 0;
          break;
        case t_BLINK:
          rgb_blinking = true;
          Serial.println("here");
          break;
        case t_SET:
          tok_pos++;
          switch(*tok_pos){
            case t_BLINK:
              rgb_blinking = true;
              rgb_blink = blink_rate;
              break;
            default:
              break;
          }
          break;
            
      }
      break;
    case t_VERSION:
      Serial.println(version);
      break;
    case t_HELP:
      help();
      break;
    case t_GET:
    tok_pos++;
      switch(*tok_pos){
        case t_TIME:
          print_time();
          break;
        case t_TEMPERATURE:
          tok_pos++;
          switch(*tok_pos){
            case t_FAREN:
              print_temp_faren();
              break;
            default:
              print_temp();
              break;
          }
          break;
        case t_HUMIDITY:
          print_humidity();
          break;
        case t_HISTORY:
          read_history();
          break;
        case t_MAXMIN:
          Serial.print(F("Min Temp: ")); Serial.println(EEPROM.read(2));
          Serial.print(F("Max Temp: ")); Serial.println(EEPROM.read(1));
          break;
      }
    break;
  }
}
//Prints current status of leds
void status_leds() {
  switch (d13) {
    case 170:
      Serial.println(F("Onboard LED blinking"));
      break;
    case 85:
      Serial.println(F("Onboard LED blinking"));
      break;
    case 255:
      Serial.println(F("Onboard LED on"));
      break;
    case 0:
      Serial.println(F("Onboard LED off"));
      break;
  }
  switch (display_buffer) {
    case 85:
      Serial.println(F("Dual LED green"));
      break;
    case 0:
      Serial.println(F("Dual LED off"));
      break;
    case 170:
      Serial.println(F("Dual LED red"));
      break;
    case 153:
      Serial.println(F("Dual LED blinking red green"));
      break;
    case 102:
      Serial.println(F("Dual LED blinking red green"));
      break;
    case 136:
      Serial.println(F("Dual LED blinking red"));
      break;
    case 68:
      Serial.println(F("Dual LED blinking green"));
      break;

  }
}

void set_time(char mo, char mo1, char d, char d1, char h, char h1, char m, char m1, char s, char s1) {
  char hour = (h - 48) * 10 + (h1 - 48);
  char minute = (m - 48) * 10 + (m1 - 48);
  char second = (s - 48) * 10 + (s1 - 48);
  char month = (mo - 48) * 10 + (mo1 - 48);
  char day = (d - 48) * 10 + (d1 - 48);
  rtc.setMonth(month);
  rtc.setDate(day);
  rtc.setHour(hour);
  rtc.setMinute(minute);
  rtc.setSecond(second);
}

void print_time() {
  Serial.print(rtc.getMonth(century)); Serial.print('-');
  Serial.print(rtc.getDate()); Serial.print(' ');
  Serial.print(rtc.getHour(h12, PM)); Serial.print(':');
  Serial.print(rtc.getMinute()); Serial.print(' ');
  Serial.print(rtc.getSecond()); Serial.println('s');
  Serial.print('>');
}

void save_temp_hum() {
  dht22.read(&temperature, &humidity, NULL);
  if(temperature > EEPROM.read(1)){
    EEPROM.write(1, temperature);
  }
  if(temperature < EEPROM.read(2) || EEPROM.read(2)==0){
    EEPROM.write(2, temperature);
  }
  save_temp_interval+=5000;
}

void print_temp() {
  Serial.print(F("The current temperature is: "));
  Serial.print(temperature); Serial.println(" *C");

  Serial.print('>');
}

void print_temp_faren(){
  byte temp_faren = (temperature/5)*9 + 32;
  Serial.print(F("The current temperature is: "));
  Serial.print(temp_faren); Serial.println(" *F");

  Serial.print('>');
}

void print_humidity(){
  Serial.print(F("Humidity: ")); Serial.print(humidity); Serial.println(F(" RH%"));
  Serial.print('>');
}

void save_ts_temp_hum(){

  EEPROM.write(EEPROM.read(0), byte(rtc.getMonth(century)));
  EEPROM[0]+=1;
  EEPROM.write(EEPROM.read(0), byte(rtc.getDate()));
  EEPROM[0]+=1;
  EEPROM.write(EEPROM.read(0), byte(rtc.getHour(h12, PM)));
  EEPROM[0]+=1;
  EEPROM.write(EEPROM.read(0), byte(rtc.getMinute()));
  EEPROM[0]+=1;
  EEPROM.write(EEPROM.read(0), byte(temperature));
  EEPROM[0]+=1;
  EEPROM.write(EEPROM.read(0), byte(humidity));
  EEPROM[0]+=1;
  save_temp_hum_interval+=900000;
}

void eeprom_read(){
  for(int i = 0; i < EEPROM.read(0); i++){
    Serial.println(EEPROM.read(i));
  }
}

void read_history(){
  long i = 4;
  while(i < EEPROM.read(0)){
    Serial.print(EEPROM.read(i));Serial.print('-');
    i++;
    Serial.print(EEPROM.read(i)); Serial.print(' ');
    i++;
    Serial.print(EEPROM.read(i)); Serial.print(':');
    i++;
    Serial.print(EEPROM.read(i)); Serial.print(' ');
    i++;
    Serial.print(EEPROM.read(i)); Serial.print("*C ");
    i++;
    Serial.print(EEPROM.read(i)); Serial.println("RH");
    i++;
  }
}

//Function to show what commands are available
void help() {
  Serial.println(F("List of some commands, enter what you wan to control and then an option:") );
  Serial.println(F("D13 for the on board LED: ON/OFF"));
  Serial.println(F("LED for the bicolor LED: GREEN/RED/OFF/BLINK"));
  Serial.println(F("SET BLINK to set the amount of time in miliseconds between each blink"));
  Serial.println(F("STATUS LED to check the current status of each LED"));
  Serial.println(F("VERSION to check the current iteration of your commands"));
  Serial.println(F("HELP to access this help menu"));
}
