#include <stdio.h>
#include <DS3231.h>
#include <Wire.h>
#include <SimpleDHT.h>

#define t_LED 2
#define t_ON 3
#define t_OFF 4
#define t_RED 5
#define t_GREEN 6
#define t_BLINK 7
#define t_STATUS 8
#define t_VERSION 9
#define t_HELP 10
#define t_LEDS 11
#define t_D13 12
#define t_SET 13
#define t_WORD 14
#define t_TIME 15
#define t_GET 16
#define t_COLOR 17
#define t_ADD 18
#define t_ERR 19
#define t_EOL 255

//Table of tokens
static char tok_table[16][4] = {'l', 'e', 3, t_LED,
                                'o', 'n', 2, t_ON,
                                'o', 'f', 3, t_OFF,
                                'r', 'e', 3, t_RED,
                                'g', 'r', 5, t_GREEN,
                                'b', 'l', 5, t_BLINK,
                                's', 't', 6, t_STATUS,
                                'v', 'e', 7, t_VERSION,
                                'h', 'e', 4, t_HELP,
                                'l', 'e', 4, t_LEDS,
                                's', 'e', 3, t_SET,
                                't', 'i', 4, t_TIME,
                                'g', 'e', 3, t_GET,
                                'c', 'o', 5, t_COLOR,
                                'a', 'd', 3, t_ADD,
                                'd', '1', 3, t_D13
                               };


//Current amount of tokens
static int token_amount = 16;

unsigned long input_tm;
int input_parservec;
int led_jobvec;
int d13_jobvec;
unsigned long led_interval;
unsigned long d13_interval;
unsigned long save_temp_interval;
int inx = 0;
char* buff;
unsigned long ct;
byte temperature;
byte humidity;

bool h12;
bool PM;

unsigned int led_blink; //Delay for the blink on the LED

unsigned int d13_blink; //Delay for the blink on the built in LED

//set to one if green was last pin turned on otherwise 0
int last_color = 0;

//Variable to store the delay between blinks
int blink_time_dual;

int blink_time_d13;

unsigned int blink_rate;

//Buffer for tokens loaded
char tok_buffer[4];

unsigned char display_buffer;

unsigned char d13 = 0;

char input_buffer[40];

DS3231 rtc;

SimpleDHT22 dht22(2);

//Current iteration of commands
int version = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  input_parser_init();
  led_init();
  help();
  //  rtc.begin();
  d13_init();
  rtc.setClockMode(false);
  Wire.begin();
  save_temp_hum();
  temp_init();
  set_time(2, 1, 3, 4, 5, 6);

}

void loop() {
  ct = millis();

  if (ct > input_tm) {
    input_parser();
  }
  if (ct > led_interval) {
    led_job();
  }
  if (ct > d13_interval) {
    d13_job();
  }
  if (ct > save_temp_interval) {
    save_temp_hum();
    print_temp_humidity();
    print_time();
  }
  //print_time();
}

void input_parser_init() {
  input_tm = millis() + 100;
  input_parservec = 0;
  inx = 0;
}

void input_parser() {
  //j0init();
  switch (input_parservec) {
    case 0:
      inx = 0;
      input_parservec++;
      Serial.print('>');
      break;
    case 1:
      char c;
      while (Serial.available() > 0) {
        c = Serial.read();
        if (c != '\r') {
          if (c == 127 && inx != 0) {
            Serial.print('\b');
            Serial.print(' ');
            Serial.print('\b');
            input_buffer[inx] = ' ';
            inx--;
            input_buffer[inx] = ' ';
            //Serial.println(input_buffer);
          }
          else {
            input_buffer[inx] = c;
            input_buffer[inx + 1] = '\0';
            inx++;
            Serial.print(c);
          }
        }
        //Serial.println(input_buffer[inx -1]);
      }
      buff = input_buffer;
      //Serial.println(input_buffer);
      if (c == '\r') {
        input_parservec++;
        Serial.print('\r');
      }
      break;
    case 2:
      Serial.print('\n');
      tokenizer(buff);
      input_parservec++;
      break;
    case 3:
      token_intepreter();
      input_parservec++;
      break;
    case 4:
      input_parservec = 0;
      break;
    default:
      input_parservec = 0;
      break;
  }
  input_tm += 100;
}

void led_init() {
  led_interval = millis() + 500;
  led_jobvec = 0;
  display_buffer = 0;
  led_blink = 500;
}

void led_job() {
  switch (led_jobvec) {
    case 0:
      digitalWrite(6, (display_buffer) & (1));
      digitalWrite(7, (display_buffer >> 1) & (1));
      display_buffer = rotate_byte_left(display_buffer, 2);
      //display_buffer = rotate_byte_right(display_buffer);
      break;
    default:
      led_jobvec = 0;
      break;
  }
  led_jobvec = 0;
  led_interval += led_blink;
}

void d13_init() {
  d13_interval = millis() + 500;
  d13_jobvec = 0;
  d13_blink = 500;
}

void d13_job() {
  digitalWrite(LED_BUILTIN, (d13) & (1));
  d13 = rotate_byte_left(d13, 1);
  d13_interval += d13_blink;
}

void temp_init() {
  save_temp_interval = millis() + 5000;
}

unsigned char rotate_byte_left(unsigned char b, int n) {
  return (b >> n) | (b << (8 - n));
}

unsigned char rotate_byte_right(unsigned char b) {
  return (b << 2) | (b >> 6);
}
//Takes the current input buffer and scans for tokens
//If a token is found it is put on the token buffer
void tokenizer(char* index) {
  //char *index = input_buffer;
  int letter_count = 0;
  int token_count = 0;
  char *reset_index;
  while (*index != '\0') {
    letter_count = 0;
    if (*index == ' ') {
      index++;
      //Serial.println("space hit");
    }
    else {
      reset_index = index;
      for (int i = 0; i < token_amount; i++) {
        if (tok_table[i][0] == *index) {
          index++;
          letter_count++;
          //Serial.println("first letter hit");
          if (tok_table[i][1] == *index) {
            while (*index != ' ' && *index != '\0') {
              letter_count++;
              index++;
            }
            //Serial.println("second letter hit");
            //Serial.println(letter_count);
            if (tok_table[i][2] == letter_count) {
              tok_buffer[token_count] = tok_table[i][3];
              //Serial.println("Token");
              token_count++;
              if (tok_buffer[token_count - 1] == t_BLINK && tok_buffer[token_count - 2] == t_SET) {
                tok_buffer[token_count] = t_WORD;
                token_count++;
                index++;
                blink_rate = 0;
                char temp[5];
                int i = 0;
                while (*index != ' ' && *index != '\0') {
                  temp[i] = *index;
                  index++;
                  i++;
                }
                int mult = 1;
                for (int j = i - 1; j >= 0; j--) {
                  blink_rate = blink_rate + ((int)temp[j] - 48) * mult;
                  mult = mult * 10;
                }
                // Serial.print(blink_rate);

              }
              break;
            }
          }
        }
        letter_count = 0;
        index = reset_index;
      }
      while (*index != ' ' && *index != '\0') {
        index++;
      }
    }

  }
  version++;
  tok_buffer[token_count] = t_EOL;
  //Serial.println(version);

}

void token_intepreter() {
  char *tok_pos = tok_buffer;
  switch (*tok_pos) {
    case t_LED:
      tok_pos++;
      switch (*tok_pos) {
        case t_RED:
          display_buffer = 170;
          tok_pos++;
          last_color = 0;
          switch (*tok_pos) {
            case t_GREEN:
              tok_pos++;
              switch (*tok_pos) {
                case t_BLINK:
                  display_buffer = 153;
                  break;
              }
          }
          break;
        case t_GREEN:
          display_buffer = 85;
          tok_pos++;
          last_color = 1;
          switch (*tok_pos) {
            case t_RED:
              tok_pos++;
              switch (*tok_pos) {
                case t_BLINK:
                  display_buffer = 153;
                  break;
              }
              break;
          }
          break;
        case t_SET:
          tok_pos++;
          switch (*tok_pos) {
            case t_BLINK:
              led_blink = blink_rate;
              break;
          }
          break;
        case t_BLINK:
          switch (last_color) {
            case 1:
              display_buffer = 68;
              break;
            case 0:
              display_buffer = 136;
              break;
            default:
              display_buffer = 136;
              break;
          }
          break;
        case t_OFF:
          display_buffer = 0;
          break;
      }
    case t_D13:
      tok_pos++;
      switch (*tok_pos) {
        case t_ON:
          d13 = 255;
          break;
        case t_OFF:
          d13 = 0;
          break;
        case t_BLINK:
          d13 = 170;
          break;
        case t_SET:
          tok_pos++;
          switch (*tok_pos) {
            case t_BLINK:
              d13_blink = blink_rate;
              break;
          }
          break;
      }
    case t_STATUS:
      tok_pos++;
      switch (*tok_pos) {
        case t_LEDS:
          status_leds();
          break;
      }
      break;
    case t_VERSION:
      Serial.println(version);
      break;
    case t_HELP:
      help();
      break;
  }
}
//Prints current status of leds
void status_leds() {
  switch (d13) {
    case 170:
      Serial.println(F("Onboard LED blinking"));
      break;
    case 85:
      Serial.println(F("Onboard LED blinking"));
      break;
    case 255:
      Serial.println(F("Onboard LED on"));
      break;
    case 0:
      Serial.println(F("Onboard LED off"));
      break;
  }
  switch (display_buffer) {
    case 85:
      Serial.println(F("Dual LED green"));
      break;
    case 0:
      Serial.println(F("Dual LED off"));
      break;
    case 170:
      Serial.println(F("Dual LED red"));
      break;
    case 153:
      Serial.println(F("Dual LED blinking red green"));
      break;
    case 102:
      Serial.println(F("Dual LED blinking red green"));
      break;
    case 136:
      Serial.println(F("Dual LED blinking red"));
      break;
    case 68:
      Serial.println(F("Dual LED blinking green"));
      break;

  }
}

void set_time(char h, char h1, char m, char m1, char s, char s1) {
  char hour = h * 10 + h1;
  char minute = m * 10 + m1;
  char second = s * 10 + s1;
  rtc.setHour(hour);
  rtc.setMinute(minute);
  rtc.setSecond(second);
}

void print_time() {
  Serial.print(rtc.getHour(h12, PM)); Serial.print(':');
  Serial.print(rtc.getMinute()); Serial.print(": ");
  Serial.println(rtc.getSecond());
  Serial.print('>');
}

void save_temp_hum() {
  dht22.read(&temperature, &humidity, NULL);
  save_temp_interval+=5000;
}

void print_temp_humidity() {
  Serial.print(F("The current temperature is: "));
  Serial.print(temperature); Serial.println(" *C");
  Serial.print(F("Humidity: ")); Serial.print(humidity); Serial.println(F(" RH%"));
  Serial.print('>');
}

//Function to show what commands are available
void help() {
  Serial.println(F("List of some commands, enter what you wan to control and then an option:") );
  Serial.println(F("D13 for the on board LED: ON/OFF"));
  Serial.println(F("LED for the bicolor LED: GREEN/RED/OFF/BLINK"));
  Serial.println(F("SET BLINK to set the amount of time in miliseconds between each blink"));
  Serial.println(F("STATUS LED to check the current status of each LED"));
  Serial.println(F("VERSION to check the current iteration of your commands"));
  Serial.println(F("HELP to access this help menu"));
}
