#include <stdio.h>
#include <DS3231.h>
#include <LiquidCrystal.h>
#include <EEPROM.h>
#include <FastLED.h>
#include <SimpleDHT.h>
//#include <Wire.h>
#include <EthernetUdp.h>

#define t_LED 2
#define t_ON 3
#define t_OFF 4
#define t_RED 5
#define t_GREEN 6
#define t_BLINK 7
#define t_STATUS 8
#define t_VERSION 9
#define t_HELP 10
#define t_LEDS 11
#define t_D13 12
#define t_SET 13
#define t_WORD 14
#define t_TIME 15
#define t_GET 16
#define t_COLOR 17
#define t_ADD 18
#define t_RGB 19
#define t_HUMIDITY 20
#define t_TEMPERATURE 21
#define t_HISTORY 22
#define t_MAXMIN 23
#define t_FAREN 24
#define t_UDP 25
#define t_ERR 26
#define t_EOL 255

//Table of token
static char tok_table[22][4] = {'l', 'e', 3, t_LED,
                                'o', 'n', 2, t_ON,
                                'o', 'f', 3, t_OFF,
                                'r', 'e', 3, t_RED,
                                'g', 'r', 5, t_GREEN,
                                'b', 'l', 5, t_BLINK,
                                's', 't', 6, t_STATUS,
                                'v', 'e', 7, t_VERSION,
                                'h', 'e', 4, t_HELP,
                                'l', 'e', 4, t_LEDS,
                                's', 'e', 3, t_SET,
                                't', 'i', 4, t_TIME,
                                'g', 'e', 3, t_GET,
                                'c', 'o', 5, t_COLOR,
                                'a', 'd', 3, t_ADD,
                                'd', '1', 3, t_D13,
                                'r', 'g', 3, t_RGB,
                                'h', 'u', 8, t_HUMIDITY,
                                't', 'e', 11, t_TEMPERATURE,
                                'h', 'i', 7, t_HISTORY,
                                'm', 'a', 6, t_MAXMIN,
                                'f', 'a', 5, t_FAREN
                               };


//Current amount of tokens
static int token_amount = 22;
bool century = false;
unsigned long input_tm;
int input_parservec;
int led_jobvec;
int d13_jobvec;
int rgb_jobvec;
int network_job_vec;
unsigned long led_interval;
unsigned long d13_interval;
unsigned long rgb_interval;
unsigned long save_temp_interval;
unsigned long save_temp_hum_interval;
unsigned long udp_receive_interval;
unsigned long alarm_interval;
unsigned long network_alarm_interval;
unsigned long button_job_interval;
unsigned long lcd_interval;
int inx = 0;
char* buff;
unsigned long ct;
byte temperature_byte;
byte humidity;

int temperature;

int udp_sent;

int udp_recieved;

byte red;

byte green;

byte blue;

bool rgb_pattern;

bool udp_connected;

bool log_screen;

char cursor_pos = 0;

bool h12;
bool PM;

byte history_screen = 0;

unsigned int led_blink; //Delay for the blink on the LED

unsigned int d13_blink; //Delay for the blink on the built in LED

unsigned int rgb_blink;

//set to one if green was last pin turned on otherwise 0
int last_color = 0;

//Variable to store the delay between blinks
int blink_time_dual;

int blink_time_d13;

bool rgb_blinking;

unsigned int blink_rate;

//Buffer for tokens loaded
char tok_buffer[4];

unsigned char display_buffer;

int log_index = 19;

int temp_alarm_packet;

unsigned char d13 = 0;

char input_buffer[40];

char packetBuffer[40];

bool history;

bool udp_stats;


IPAddress ip(EEPROM.read(3), EEPROM.read(4), EEPROM.read(5), EEPROM.read(6));
IPAddress subnet(EEPROM.read(7), EEPROM.read(8), EEPROM.read(9), EEPROM.read(10));
IPAddress gateway(EEPROM.read(11), EEPROM.read(12), EEPROM.read(13), EEPROM.read(14));

//IPAddress ip(192, 168, 1, 177); 
IPAddress remote(192, 168, 1, 100);
EthernetUDP Udp;

unsigned int localPort = 55056; 
unsigned int remote_port = 55056;

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};

char current_screen = 5;

//Udp Messages


DS3231 rtc;

CRGB led[3];

SimpleDHT22 dht22(2);
LiquidCrystal lcd(A1, A2, 7, 8, 9, 10);


//Current itertion of commands
int version = 0;

void setup() {
  Wire.begin();
  // put your setup code here, to run once:
  lcd.begin(16, 2);
  Serial.begin(9600);
  //lcd.begin(16, 2);
  pinMode(A0, INPUT);
  //Wire.begin();
  rtc.setClockMode(false);
  //pinMode(6, OUTPUT);
  //pinMode(7, OUTPUT);
  //pinMode(LED_BUILTIN, OUTPUT);
  input_parser_init();
  led_init();
  d13_init();
  save_temp_init();
  udp_receive_init();
  save_temp_hum_init();
  alarm_init();
  network_alarm_init();
  button_job_init();
  //help();
  //  rtc.begin();
  rgb_init();
  //set_time(1, 1, 0, 5, 1, 1, 2, 4, 0, 0, 2, 0);
  FastLED.addLeds<NEOPIXEL, 3>(led, 3);
  Ethernet.init(4); 
  Ethernet.begin(mac, ip);
  Udp.begin(localPort);
  lcd_job_init();


}

void loop() {

  ct = millis();

  if (ct > input_tm) {
    input_parser();
  }
  if (ct > led_interval) {
    led_job();
  }
  if (ct > d13_interval) {
    d13_job();
  }
  if(ct > udp_receive_interval){
    udp_receive_job();
  }
  if(ct > rgb_interval){
    rgb_job();
  }
  if(ct > save_temp_interval){
    save_temp_hum();
  }
  if(ct > save_temp_hum_interval){
    save_ts_temp_hum();
  }
  if(ct > alarm_interval){
    alarm_job();
  }
  if(ct > network_alarm_interval){
    network_alarm_job();
  }
  if(ct>button_job_interval){
    button_job();
  }
  if(ct>lcd_interval){
    lcd_job();
  }
  //Serial.println(analogRead(A0));

  FastLED.show();

}

void lcd_job_init(){
  current_screen = 1;
  lcd_interval = millis() + 100;
}

void lcd_job(){
  if(history){
      lcd.clear();
      lcd.setCursor(0,0);
    if(cursor_pos < 4){
      lcd.print("IP");
      lcd.setCursor(0,1);
        for(int i = 3; i < 6; i++){
          lcd.print(EEPROM.read(i)); lcd.print('.');
        }
        lcd.print(EEPROM.read(6));
    }
    else if(cursor_pos < 8){
      lcd.print("Subnet Mask");
      lcd.setCursor(0,1);
        for(int i = 7; i < 10; i++){
          lcd.print(EEPROM.read(i)); lcd.print('.');
        }
        lcd.print(EEPROM.read(10));
    }
    else if(cursor_pos < 12){
      lcd.print("Gateway");
      lcd.setCursor(0,1);
        for(int i = 11; i < 14; i++){
          lcd.print(EEPROM.read(i)); lcd.print('.');
        }
        lcd.print(EEPROM.read(14));
    }
    else if(cursor_pos < 14){
      lcd.print("Temp Alarm Under");
      lcd.setCursor(0,1);
      lcd.print("MAU: "); lcd.print(EEPROM.read(15));
      lcd.print(" MIU: "); lcd.print(EEPROM.read(16));
    }
    else if(cursor_pos < 16){
      lcd.print("Temp Alarm Over");
      lcd.setCursor(0,1);
      lcd.print("MIO: "); lcd.print(EEPROM.read(17));
      lcd.print(" MAO: "); lcd.print(EEPROM.read(18));
    }
    else if(cursor_pos ==16){
      lcd.print("Erase Log?");
      lcd.setCursor(0,1);
      lcd.print("Down = Yes");
      if(current_screen == 3){
        delete_history();
        cursor_pos = 0;
      }
    }
    else if(cursor_pos > 16){
      cursor_pos = 0;
    }
    switch(current_screen){
      case 2:
        EEPROM[3 + cursor_pos]++;
        break;
      case 3:
        EEPROM[3 + cursor_pos]--;
        break;
      case 4:
        cursor_pos++;
        break;
      default:
        break;
    }
  }

  else if(udp_stats){
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("UDP Rec: "); lcd.print(udp_recieved);
    lcd.setCursor(0,1);
    lcd.print("UDP sent: "); lcd.print(udp_sent);
  }

  else if(log_screen){
    lcd.clear();
    switch(current_screen){
      case 2:
        log_index+=7;
        break;
      case 3:
        log_index-=7;
        break;
      default:
        break;
    }
    log_index= log_index*(log_index >=19 && log_index <= EEPROM.read(0)) + 19*(log_index <19 && log_index > EEPROM.read(0));
    lcd.setCursor(0,0);
    lcd.print(EEPROM.read(log_index));lcd.print('-');lcd.print(EEPROM.read(log_index+1));lcd.print(' ');
    lcd.print(EEPROM.read(log_index+2));lcd.print(':');
    if(EEPROM.read(log_index+3) < 10){lcd.print('0');} lcd.print(EEPROM.read(log_index+3));
    lcd.print(' '); lcd.print(EEPROM.read(log_index+4));
    lcd.setCursor(0,1);
    lcd.print(EEPROM.read(log_index+5));lcd.print("*C ");
    lcd.print(EEPROM.read(log_index+6));lcd.print("RH");
  }
  
  else{
  switch(current_screen){

    case 5:
    lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Temperature: "); lcd.print(temperature);
      lcd.setCursor(0, 1);
      lcd.print("Humidity: "); lcd.print(humidity);
      break;
    default:
      break;
  }
  }
  current_screen = 0;
  lcd_interval += 200;
}

void input_parser_init() {
  input_tm = millis() + 100;
  input_parservec = 0;
  inx = 0;
}

void input_parser() {
  //j0init();
  switch (input_parservec) {
    case 0:
      inx = 0;
      input_parservec++;
      Serial.print('>');
      break;
    case 1:
      char c;
      while (Serial.available() > 0) {
        c = Serial.read();
        if (c != '\r') {
          if (c == 127 && inx != 0) {
            Serial.print('\b');
            Serial.print(' ');
            Serial.print('\b');
            input_buffer[inx] = ' ';
            inx--;
            input_buffer[inx] = ' ';
            //Serial.println(input_buffer);
          }
          else {
            input_buffer[inx] = c;
            input_buffer[inx + 1] = '\0';
            inx++;
            Serial.print(c);
          }
        }
        //Serial.println(input_buffer[inx -1]);
      }
      buff = input_buffer;
      //Serial.println(input_buffer);
      if (c == '\r') {
        input_parservec++;
        Serial.print('\r');
      }
      break;
    case 2:
      Serial.print('\n');
      tokenizer(buff);
      input_parservec++;
      break;
    case 3:
      token_intepreter();
      input_parservec++;
      break;
    case 4:
      input_parservec = 0;
      break;
    default:
      input_parservec = 0;
      break;
  }
  input_tm += 50;
}

void led_init() {
  led_interval = millis() + 500;
  led_jobvec = 0;
  display_buffer = 0;
  led_blink = 500;
}

void save_temp_hum_init(){
  //EEPROM[0] = 3;
  //save_ts_temp_hum();
  save_temp_hum_interval = millis() + 900000;
}

void save_temp_init(){
  save_temp_hum();
  save_temp_interval = millis() + 5000;
}

void udp_receive_init(){
  udp_receive_interval = millis() + 200;
}

void udp_receive_job(){
  char x = Udp.parsePacket();
  if(x){
    remote = Udp.remoteIP();
    remote_port = Udp.remotePort();
    Udp.read(packetBuffer, 40);
    //buff = packetBuffer;
   // Serial.println(packetBuffer);
    tokenizer(packetBuffer);
    token_intepreter();
    if(packetBuffer[0] != '0'){udp_recieved++;Serial.print('>');}
    Udp.beginPacket(remote, remote_port);
    Udp.write("connected");
    Udp.endPacket();
    //udp_sent++;
  }
  udp_receive_interval+=200;
}

void led_job() {
  switch (led_jobvec) {
    case 0:
      digitalWrite(6, (display_buffer) & (1));
      digitalWrite(7, (display_buffer >> 1) & (1));
      display_buffer = rotate_byte_left(display_buffer, 2);
      //display_buffer = rotate_byte_right(display_buffer);
      break;
    default:
      led_jobvec = 0;
      break;
  }
  led_jobvec = 0;
  led_interval += led_blink;
}

void rgb_init(){
  rgb_jobvec = 0;
  rgb_interval = millis() + 500;
  rgb_blink = 500;
}

void alarm_init(){
  alarm_interval = millis() + 500;
}

void alarm_job(){

  if(temperature <= EEPROM.read(15)){
    alarm_udp(1);
  }
  else if(temperature < EEPROM.read(16)){ 
    alarm_udp(2);
  }
  else if(temperature < EEPROM.read(17)){
    alarm_udp(3);
  }
  else if(temperature < EEPROM.read(18)){
    alarm_udp(4);
  }
  else if(temperature >= EEPROM.read(18)){
    alarm_udp(5);
  }
  alarm_interval+=1500;
}

void alarm_udp(int state){
  if(state != temp_alarm_packet){
    Udp.beginPacket(remote, remote_port);
    temp_alarm_packet = state;
    switch(state){
      case 1:
        Udp.write("Major under");
        Udp.endPacket();
        break;
      case 2:
        Udp.write("Minor under");
        Udp.endPacket();
        break;
      case 3:
        Udp.write("Comfortable");
        Udp.endPacket();
        break;
      case 4:
        Udp.write("Minor over");
        Udp.endPacket();
        break;
      case 5:
        Udp.write("Major over");
        Udp.endPacket();
        break;
      default:
        break;
    }
    udp_sent++;
  }
}

void network_alarm_init(){
  network_job_vec = 0;
  network_alarm_interval = millis() + 600;
}

void network_alarm_job(){
  int x = 0;
  //switch(network_job_vec){
    //case 0:
      Udp.beginPacket(remote, remote_port);
      Udp.write("connected");
      Udp.endPacket();
      network_job_vec++;
      //break;
    //case 1:
      x = Udp.parsePacket();
      if(x){
        led[2] = CRGB(100, 250, 0);
        //FastLED.show();
      }
      else {
        led[2] = CRGB(0, 0, 0);
        udp_connected = false;
        //FastLED.show();
      }
      network_job_vec++;
      //break;
    //case 2:
      //network_job_vec = 0;
      //break;
   // default:
     // network_job_vec = 0;
      //break; 
  //}

  network_alarm_interval+=2000;
}

void rgb_job(){
  switch(rgb_jobvec){
    case 0:
      //code to light led based on input values
      switch(rgb_blinking){
        case false:
          led[0] = CRGB(red, green, blue);
          //FastLED.show();
          break;
        case true:
          led[0]= CRGB(red*rgb_pattern, green*rgb_pattern, blue*rgb_pattern);
          //FastLED.show();
          rgb_pattern = (rgb_pattern == false) * 1;
          break;
      }
      break;
    default:
      rgb_jobvec = 0;
      rgb_interval += rgb_blink;
      break;
  }
  led[1] = CHSV(hue(temperature), 255, 255);
  FastLED.show();
  rgb_interval += rgb_blink;
  rgb_jobvec = 0;
}

void d13_init() {
  d13_interval = millis() + 500;
  d13_jobvec = 0;
  d13_blink = 500;
}

void d13_job() {
  digitalWrite(LED_BUILTIN, (d13) & (1));
  d13 = rotate_byte_left(d13, 1);
  d13_interval += d13_blink;
}

unsigned char rotate_byte_left(unsigned char b, int n) {
  return (b >> n) | (b << (8 - n));
}

unsigned char rotate_byte_right(unsigned char b) {
  return (b << 2) | (b >> 6);
}

void button_job_init(){
  button_job_interval = millis() + 100;
}

void button_job(){
  int button_pressed = analogRead(A0);
  if(button_pressed < 20){ //left
    if(!history && !udp_stats && !log_screen){
      log_screen = true;
    }
    else{
      current_screen = 1;
    }
    button_job_interval+=200;
  }
  else if(button_pressed < 160){ //up
    //Serial.println(button_pressed);
    if(!log_screen && !udp_stats && !history){
      history = true;
    }
    else{
      current_screen = 2;
    }
    button_job_interval+=200;
  }
  else if(button_pressed <350) { // down
    //Serial.println(button_pressed);
    current_screen = 3;
    button_job_interval+=200;
  }
  else if(button_pressed < 550){ //right
    if(!history && !log_screen && !udp_stats){
      udp_stats = true;
    }
    else{
      current_screen = 4;
    }
    button_job_interval+=200;
  }
  else if(button_pressed < 770){ //select
    //Serial.println(button_pressed);
    current_screen = 5;
    cursor_pos = 0;
    log_index = 19;
    history = false;
    udp_stats = false;
    log_screen = false;
    button_job_interval+=200;
  }
  else{
    current_screen = 0;
    button_job_interval+=50;
  }
}
//Takes the current input buffer and scans for tokens
//If a token is found it is put on the token buffer
void tokenizer(char* index) {
  //char *index = input_buffer;
  int letter_count = 0;
  int token_count = 0;
  char *reset_index;
  while (*index != '\0') {
    letter_count = 0;
    if (*index == ' ') {
      index++;
      //Serial.println("space hit");
    }
    else {
      reset_index = index;
      for (int i = 0; i < token_amount; i++) {
        if (tok_table[i][0] == *index) {
          index++;
          letter_count++;
          //Serial.println("first letter hit");
          if (tok_table[i][1] == *index) {
            while (*index != ' ' && *index != '\0') {
              letter_count++;
              index++;
            }
            //Serial.println("second letter hit");
            //Serial.println(letter_count);
            if (tok_table[i][2] == letter_count) {
              tok_buffer[token_count] = tok_table[i][3];
              //Serial.println("Token");
              token_count++;
              if (tok_buffer[token_count - 1] == t_BLINK && tok_buffer[token_count - 2] == t_SET) {
                tok_buffer[token_count] = t_WORD;
                token_count++;
                index++;
                blink_rate = 0;
                char temp[5];
                int i = 0;
                while (*index != ' ' && *index != '\0') {
                  temp[i] = *index;
                  index++;
                  i++;
                }
                int mult = 1;
                for (int j = i - 1; j >= 0; j--) {
                  blink_rate = blink_rate + ((int)temp[j] - 48) * mult;
                  mult = mult * 10;
                }
                // Serial.print(blink_rate);

              }
              if(tok_buffer[token_count -1] == t_COLOR && tok_buffer[token_count -2] == t_SET && tok_buffer[token_count -3] == t_RGB){
                int color[3];
                char temp[3];
                long value = 0;
                int colors = 0;
                int mult = 1;
                int inx = 0;
                //index++;
                while(*index != '\0' && *index != '\n' && colors < 3){
                  if(*index != ' '){
                  while(*index != ' ' && *index != '\0'){
                    temp[inx] = *index;
                    //Serial.print(temp[inx]);
                    inx++;
                    index++;  
                  }
                  for (int j = inx - 1; j >= 0; j--) {
                    value = value + ((int)temp[j] - 48) * mult;
                    mult = mult * 10;
                  }
                  if(value > 255 || inx > 3){ value = 255; }
                  color[colors] = value;
                  colors++;
                  mult = 1;
                  inx = 0;
                  value = 0;
                  }
                  index++;
                }
                
                red = color[0];
                green= color[1];
                blue = color[2];
              }
              if(tok_buffer[token_count - 1] == t_ADD){
                int number1 = 0;
                int number2 = 0;
                int n1[7];
                int n2[7];
                bool n1_negative = false;
                bool n2_negative = false;
                int i = 0;
                int mult = 1;
                index++;
                while(*index != ' '){
                  if(*index == '-'){
                    n1_negative = true;
                    index++;
                  }
                  n1[i] = *index;
                  i++;
                  index++;
                }
                for (int j = i - 1; j >= 0; j--) {
                  number1 = number1 + (n1[j] - 48) * mult;
                  mult = mult * 10;
                }
                mult = 1;
                i = 0;
                index++;
                while(*index != '\0' && *index != ' '){
                  if(*index == '-'){
                    n2_negative = true;
                    index++;
                  }
                  n2[i] = *index;
                  i++;
                  index++;
                }
                for (int j = i - 1; j >= 0; j--) {
                  number2 = number2 + (n2[j] - 48) * mult;
                  mult = mult * 10;

                }
                if(n2_negative){
                  number2 = -number2;
                }
                if(n1_negative){
                  number1 = -number1;
                }
                Udp.beginPacket(remote, remote_port);
                Serial.print(number1);Serial.print(" + "); Serial.print(number2);Serial.print(" = "); Serial.println(number1 + number2);
                Udp.print(number1); Udp.print(" + "); Udp.print(number2); Udp.print(" = "); Udp.print(number1+number2);
                Udp.endPacket();
                udp_sent++;
                
              }
              if(tok_buffer[token_count -1] == t_TIME && tok_buffer[token_count - 2] == t_SET){
                char times[12];
                //Serial.println("here");
                int i = 0;
                while(*index != '\0' && i <= 10){
                  while(*index != ' ' && *index != '\0' && *index != '\n'){
                    times[i] = *index;
                    //Serial.println(times[i]);
                    i++;
                    index++;
                  }
                  index++;
                }
                if(i <=10){
                  set_time(char(times[0]), char(times[1]), times[2], times[3], times[4], times[5], times[6], times[7], times[8], times[9], times[10], times[11]);
                }
              }
              break;
            }
          }
        }
        letter_count = 0;
        index = reset_index;
      }
      while (*index != ' ' && *index != '\0') {
        index++;
      }
    }

  }
  version++;
  tok_buffer[token_count] = t_EOL;
  //Serial.println(version);

}

void token_intepreter() {
  char *tok_pos = tok_buffer;
  switch (*tok_pos) {
    case t_LED:
      tok_pos++;
      switch (*tok_pos) {
        case t_RED:
          display_buffer = 170;
          tok_pos++;
          last_color = 0;
          switch (*tok_pos) {
            case t_GREEN:
              tok_pos++;
              switch (*tok_pos) {
                case t_BLINK:
                  display_buffer = 153;
                  break;
              }
          }
          break;
        case t_GREEN:
          display_buffer = 85;
          tok_pos++;
          last_color = 1;
          switch (*tok_pos) {
            case t_RED:
              tok_pos++;
              switch (*tok_pos) {
                case t_BLINK:
                  display_buffer = 153;
                  break;
              }
              break;
          }
          break;
        case t_SET:
          tok_pos++;
          switch (*tok_pos) {
            case t_BLINK:
              led_blink = blink_rate;
              break;
          }
          break;
        case t_BLINK:
          switch (last_color) {
            case 1:
              display_buffer = 68;
              break;
            case 0:
              display_buffer = 136;
              break;
            default:
              display_buffer = 136;
              break;
          }
          break;
        case t_OFF:
          display_buffer = 0;
          break;
      }
    case t_D13:
      tok_pos++;
      switch (*tok_pos) {
        case t_ON:
          d13 = 255;
          break;
        case t_OFF:
          d13 = 0;
          break;
        case t_BLINK:
          d13 = 170;
          break;
        case t_SET:
          tok_pos++;
          switch (*tok_pos) {
            case t_BLINK:
              d13_blink = blink_rate;
              break;
          }
          break;
      }
    case t_STATUS:
      tok_pos++;
      switch (*tok_pos) {
        case t_LEDS:
         // status_leds();
          break;
      }
      break;
    case t_RGB:
      tok_pos++;
      switch(*tok_pos){
        case t_OFF:
          red = 0;
          green = 0;
          blue = 0;
          break;
        case t_BLINK:
          rgb_blinking = true;
          break;
        case t_SET:
          tok_pos++;
          switch(*tok_pos){
            case t_BLINK:
              rgb_blinking = true;
              rgb_blink = blink_rate;
              break;
            default:
              break;
          }
          break;
            
      }
      break;
    case t_VERSION:
      Udp.beginPacket(remote, remote_port);
      Serial.println(version);
      Udp.print(version);
      Udp.endPacket();
      udp_sent++;
      break;
    case t_HELP:
      //help();
      break;
    case t_GET:
    tok_pos++;
      switch(*tok_pos){
        case t_TIME:
          print_time();
          break;
        case t_TEMPERATURE:
          tok_pos++;
          switch(*tok_pos){
            case t_FAREN:
              print_temp_faren();
              break;
            default:
              print_temp();
              break;
          }
          break;
        case t_HUMIDITY:
          print_humidity();
          break;
        case t_HISTORY:
          read_history();
          break;
        case t_MAXMIN:
          Udp.beginPacket(remote, remote_port);
          Serial.print(F("Min Temp: ")); Serial.println(EEPROM.read(2));
          Udp.print(F("Min Temp: ")); Udp.print(EEPROM.read(2));
          Serial.print(F("Max Temp: ")); Serial.println(EEPROM.read(1));
          Udp.print(F("Max Temp: ")); Udp.print(EEPROM.read(1));
          Udp.endPacket();
          udp_sent++;
          break;
      }
    break;
  }
}
//Prints current status of leds
/*void status_leds() {
  Udp.beginPacket(remote, remote_port);
  switch (d13) {
    case 170:
      Serial.println(F("Onboard LED blinking"));
      Udp.print(F("Onboad LED blinking"));
      break;
    case 85:
      Serial.println(F("Onboard LED blinking"));
      Udp.print(F("Onboad LED blinking"));
      break;
    case 255:
      Serial.println(F("Onboard LED on"));
      Udp.print(F("Onboad LED on"));
      break;
    case 0:
      Serial.println(F("Onboard LED off"));
      Udp.print(F("Onboad LED on"));
      break;
  }
  switch (display_buffer) {
    case 85:
      Serial.println(F("Dual LED green"));
      Udp.print(F("Dual LED green"));
      break;
    case 0:
      Serial.println(F("Dual LED off"));
      Udp.print(F("Dual LED off"));
      break;
    case 170:
      Serial.println(F("Dual LED red"));
      Udp.print(F("Dual LED red"));
      break;
    case 153:
      Serial.println(F("Dual LED blinking red green"));
      Udp.print(F("Dual LED blinking red green"));
      break;
    case 102:
      Serial.println(F("Dual LED blinking red green"));
      Udp.print(F("Dual LED blinking red green"));
      break;
    case 136:
      Serial.println(F("Dual LED blinking red"));
      Udp.print(F("Dual LED blinking red"));
      break;
    case 68:
      Serial.println(Udp.print(F("Dual LED blinking green")));
      Udp.print(F("Dual LED blinking green"));
      break;

  }
  Udp.endPacket();
  udp_sent++;
}*/

void set_time(char mo, char mo1, char d, char d1, char h, char h1, char m, char m1, char s, char s1, char y, char y1) {
  char hour = (h - 48) * 10 + (h1 - 48);
  char minute = (m - 48) * 10 + (m1 - 48);
  char second = (s - 48) * 10 + (s1 - 48);
  char month = (mo - 48) * 10 + (mo1 - 48);
  char day = (d - 48) * 10 + (d1 - 48);
  char year = (y - 48) *10 + (y1 - 48);
  rtc.setMonth(month);
  rtc.setDate(day);
  rtc.setHour(hour);
  rtc.setMinute(minute);
  rtc.setSecond(second);
  rtc.setYear(20);
}

void print_time() {
  Udp.beginPacket(remote, remote_port);
  Serial.print(rtc.getMonth(century)); Serial.print('-');
  Serial.print(rtc.getDate()); Serial.print(' ');
  Serial.print(rtc.getHour(h12, PM)); Serial.print(':');
  if(rtc.getMinute() < 10){ Serial.print('0');}
  Serial.print(rtc.getMinute()); Serial.print(' ');
  Serial.print(rtc.getSecond()); Serial.print("s ");
  Serial.println(rtc.getYear());
  
  Udp.print(rtc.getMonth(century)); Udp.print('-');
  Udp.print(rtc.getDate()); Udp.print(' ');
  Udp.print(rtc.getHour(h12, PM)); Udp.print(':');
  if(rtc.getMinute() < 10) { Udp.print('0');}
  Udp.print(rtc.getMinute()); Udp.print(' ');
  Udp.print(rtc.getSecond()); Udp.print('s');
  Udp.endPacket();
  udp_sent++;
}

void save_temp_hum() {
  dht22.read(&temperature_byte, &humidity, NULL);
  temperature = temperature_byte;
  temperature = ((temperature > 127)*(-temperature + 127)) + (temperature <=127)*temperature;
  if(temperature > EEPROM.read(1)){
    EEPROM.write(1, temperature);
  }
  if(temperature < EEPROM.read(2) || EEPROM.read(2)==0){
    EEPROM.write(2, temperature);
  }
  save_temp_interval+=5000;
}

void delete_history(){
  for(int i = 19; i <EEPROM.read(0); i++){
    EEPROM.write(i, 0);
  }
  EEPROM.write(0,19);
}

void print_temp() {
  Udp.beginPacket(remote, remote_port);
  Serial.print(F("The current temperature is: "));
  Serial.print(temperature); Serial.println(" *C");
  Udp.print(F("The current temperature is: "));
  Udp.print(temperature); Udp.print(" *C");
  Udp.endPacket();
  udp_sent++;
}

void print_temp_faren(){
  Udp.beginPacket(remote, remote_port);
  byte temp_faren = (temperature/5)*9 + 32;
  Serial.print(F("The current temperature is: "));
  Serial.print(temp_faren); Serial.println(" *F");
  Udp.print(F("The current temperature is: "));
  Udp.print(temp_faren); Udp.print(" *F");
  Udp.endPacket();
  udp_sent++;
}

void print_humidity(){
  Udp.beginPacket(remote, remote_port);
  Serial.print(F("Humidity: ")); Serial.print(humidity); Serial.println(F(" RH%"));
  Udp.print(F("Humidity: ")); Udp.print(humidity); Udp.print(F(" RH%"));
  Udp.endPacket();
  udp_sent++;
}

void save_ts_temp_hum(){

  EEPROM.write(EEPROM.read(0), rtc.getMonth(century));
  EEPROM[0]++;
  EEPROM.write(EEPROM.read(0), rtc.getDate());
  EEPROM[0]++;
  EEPROM.write(EEPROM.read(0), rtc.getHour(h12, PM));
  EEPROM[0]++;
  EEPROM.write(EEPROM.read(0), rtc.getMinute());
  EEPROM[0]++;
  EEPROM.write(EEPROM.read(0), byte(rtc.getYear()));
  EEPROM[0]++;
  EEPROM.write(EEPROM.read(0), byte(temperature));
  EEPROM[0]++;
  EEPROM.write(EEPROM.read(0), byte(humidity));
  EEPROM[0]++;
  save_temp_hum_interval+=900000;
}

void eeprom_read(){
  //Udp.beginPacket(remote, remote_port);
  for(int i = 0; i < EEPROM.read(0); i++){
    Serial.println(EEPROM.read(i));
    //Udp.write(i);
  }
  //Udp.endPacket();
}

void read_history(){
  long i = 19;
  Udp.beginPacket(remote, remote_port);
  while(i < EEPROM.read(0)){
    Serial.print(EEPROM.read(i));Serial.print('-');
    Udp.print(EEPROM.read(i)); Udp.print('-');
    i++;
    Serial.print(EEPROM.read(i)); Serial.print(' ');
    Udp.print(EEPROM.read(i)); Udp.print(' ');
    i++;
    Serial.print(EEPROM.read(i)); Serial.print(':');
    Udp.print(EEPROM.read(i)); Udp.print(':');
    i++;
    Serial.print(EEPROM.read(i)); Serial.print(' ');
    Udp.print(EEPROM.read(i)); Udp.print(' ');
    i++;
    Serial.print(EEPROM.read(i)); Serial.print(' ');
    Udp.print(EEPROM.read(i)); Udp.print(' ');
    i++;
    Serial.print(EEPROM.read(i)); Serial.print("*C ");
    Udp.print(EEPROM.read(i)); Udp.print("*C ");
    i++;
    Serial.print(EEPROM.read(i)); Serial.println("RH");
    Udp.print(EEPROM.read(i)); Udp.print("RH");
    i++;
  }
  Udp.endPacket();
  udp_sent++;
  udp_sent++;
}

int hue(int t){
  int out =(t>16)*(-12 * t + 384) + (t<=16)*192;
  return out;
}
